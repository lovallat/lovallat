# Hi there 👋

> Looks like you just landed on my ✨ GitLab profile ✨!

I'm glad you found me! This is the home of all my (very) diverse projects!

I'm a young french 🇫🇷 guy who **loves** coding. Currently, I'm a Computer Scien
Engineering School 💼 student at [ENSEIRB-MATMECA](https://enseirb-matmeca.bordeaux-inp.fr/fr),
near Bordeaux in France. I also enjoy taking pictures 📸 and spending most of my
free time on side projects.

I code in Rust, Java, C, PHP, etc... Right now, I'm really fond of Rust 🦀 !

I also love Open Source and its philosophies, as well as using Continuous Integration
and Continuous Deployment ⚡, one of the reasons why I switched from GitHub to GitLab.
I also self-host a lot of services, for my own and family usages, and I try to
protect my privacy 🛡.

You can learn more on my personal website 🌍 [here](https://louis-vallat.fr).

You can find my git flow [here](https://blog.louis-vallat.fr/git-flow/) to
see how I use it.

Please feel free to open an issue or a Merge Request if something seems odd with
any of my repository!

I also have a 🧭 [GitHub profile](https://github.com/LouisVallat) where I contribute
to projects not available here on GitLab.

## Contact

If you have any question, feel free to 📨 [contact me](mailto:contact@louis-vallat.fr).

